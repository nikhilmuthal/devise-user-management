class AddPassChangeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :pass_change, :boolen, :default => false
  end
end
